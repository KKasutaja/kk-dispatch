local framework = 'NONE'

CreateThread(function()
    if GetResourceState('es_extended') == 'starting' or GetResourceState('es_extended') == 'started' then
        framework = 'ESX'
    end
    
    if GetResourceState('qb-core') == 'starting' or GetResourceState('qb-core') == 'started' then
        framework = 'QBCORE'
    end
        
    if GetResourceState('kk-core') == 'starting' or GetResourceState('kk-core') == 'started' then
        framework = 'KKF'
    end
end)

CreateThread(function()
    if framework == 'ESX' then
        ESX = exports['es_extended']:getSharedObject()

        function getPlayerFromId(source)
            local xPlayer = ESX.GetPlayerFromId(source)

            return xPlayer
        end

        function hasItem(source, name)
            local xPlayer = getPlayerFromId(source)

            return xPlayer.getInventoryItem(name).count > 0
        end

        function getPlayers(...)
            return ESX.GetExtendedPlayers(...)
        end

        function isPlayerDuty(source)
            return true
        end

        function canCall(source)
            local kPlayer = getPlayerFromId(source)
            
            return kPlayer.getInventoryItem('phone').count > 0
        end

        function getPlayerPhone(source)
            return 'UNKNOWN'
        end

        function getCallsign(source)
            return 0
        end

        function showNotification(source, type, msg)
            TriggerClientEvent('ESX:Notify', source, type or 'info', 3000, msg)
        end

        function chatMessage(source, title, msg)
            TriggerClientEvent('chat:addMessage', source, {
                color = { 255, 0, 0 },
                multiline = true,
                args = {title, msg}
            })
        end

        function getRadioChannel(source)
            return Player(source).state.radioChannel
        end

        AddEventHandler('esx:playerLoaded', function(playerId)
            TriggerEvent('kk-dispatch:server:playerLoaded', playerId)
        end)

        AddEventHandler('esx:playerDropped', function(playerId)
            TriggerEvent('kk-dispatch:server:playerDropped', playerId)
        end)
    elseif framework == 'QBCORE' then
        QBCore = exports['qb-core']:GetCoreObject()
        
        function getPlayerFromId(source)
            local Player = QBCore.Functions.GetPlayer(source)

            return {
                source = Player.PlayerData.source,
                identifier = Player.PlayerData.citizenid,
                name = Player.PlayerData.charinfo.firstname .. ' ' .. Player.PlayerData.charinfo.lastname,
                job = Player.PlayerData.job,
                metadata = Player.PlayerData.metadata,
                charinfo = Player.PlayerData.charinfo
            }
        end

        function hasItem(source, name)
            return exports['qb-inventory']:HasItem(source, name) 
        end

        function getPlayers(key, val)
            local returnable = {}

            for k, v in pairs(QBCore.Functions.GetPlayers()) do
                local Player = getPlayerFromId(v)

                if key then
                    if key == 'job' and Player.job.name == val then
                        returnable[#returnable + 1] = Player
                    end
                else
                    returnable[#returnable + 1] = Player
                end
            end
        
            return returnable
        end

        function isPlayerDuty(source)
            local kPlayer = getPlayerFromId(source)

            return kPlayer.job.onduty
        end

        function canCall(source)
            return hasItem(source, 'phone')
        end

        function getPlayerPhone(source)
            local kPlayer = getPlayerFromId(source)

            return kPlayer.charinfo.phone
        end

        function getCallsign(source)
            local kPlayer = getPlayerFromId(source)
        
            return kPlayer.metadata.callsign or 'PUUDUB'
        end

        function showNotification(source, type, msg)
            TriggerClientEvent('QBCore:Notify', source, msg, type or 'info')
        end

        function chatMessage(source, title, msg)
            TriggerClientEvent('chat:addMessage', source, {
                template = "<div class=chat-message server'><strong>{0}</strong> | {1}</div>",
                args = { title, msg }
            })
        end

        function getRadioChannel(source)
            return Player(source).state.radioChannel
        end

        AddEventHandler('QBCore:Server:PlayerLoaded', function(data)
            TriggerEvent('kk-dispatch:server:playerLoaded', data.PlayerData.source)
        end)

        AddEventHandler('playerDropped', function()
            TriggerEvent('kk-dispatch:server:playerDropped', source)
        end)
    elseif framework == 'KKF' then
        KKF = exports['kk-core']:getSharedObject()

        function getPlayerFromId(source)
            local kPlayer = KKF.GetPlayerFromId(source)

            if kPlayer then
                return kPlayer
            else
                return false
            end
        end

        function hasItem(source, name)
            return exports.ox_inventory:Search(source, 'count', name) > 0
        end

        function getPlayers(...)
            return KKF.GetPlayers(...)
        end

        function isPlayerDuty(source)
            local kPlayer = getPlayerFromId(source)

            if kPlayer then
                return kPlayer.job.onDuty
            else
                return false
            end
        end

        function canCall(source)
            local kPlayer = getPlayerFromId(source)
            
            if kPlayer then
                return (hasItem(kPlayer.source, 'phone') and exports['lb-phone']:GetEquippedPhoneNumber(kPlayer.source)) and (not Player(kPlayer.source).state.isCuffed and not Player(kPlayer.source).state.isDead)
            else
                return false
            end 
        end

        function getPlayerPhone(source)
            local kPlayer = getPlayerFromId(source)

            if kPlayer then
                return exports['lb-phone']:GetEquippedPhoneNumber(kPlayer.source)
            else
                return false
            end
        end

        function getCallsign(source)
            local kPlayer = getPlayerFromId(source)

            if kPlayer then
                local data = exports['kk-mdt']:getDepartment(kPlayer.identifier, kPlayer.job.name)

                if data then
                    return data.callsign or 'XXX'
                else
                    return 'XXX'
                end
            else
                return 'XXX'
            end
        end

        function showNotification(source, type, msg)
            TriggerClientEvent('KKF.UI.ShowNotification', source, type, msg)
        end

        function chatMessage(source, title, msg)
            TriggerClientEvent('chatMessage', source, title, 'warning', msg)
        end

        function getRadioChannel(source)
            return Player(source).state.radioChannel
        end

        AddEventHandler('KKF.Player.Loaded', function(playerId)
            TriggerEvent('kk-dispatch:server:playerLoaded', playerId)
        end)

        AddEventHandler('KKF.Player.Dropped', function(playerId)
            TriggerEvent('kk-dispatch:server:playerDropped', playerId)
        end)
    end

    function onResponse(source, code)
        -- On responsable call respond
        if code == '10-XX' then
            TriggerClientEvent('example', source)
        end
    end
end)

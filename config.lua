cfg = {}

cfg.keybind = 'Z'

cfg.allowedFactions = {
    ['police'] = {
        command = 'police',
        radio = true
    },

    ['ambulance'] = {
        command = 'ambulance',
        radio = true
    },

    ['taxi'] = {
        command = 'taxi',
        radio = false
    }
}

cfg.language = 'en'

cfg.enableCells = true -- Phone commads
cfg.debug = false -- debug below

cfg.translations = {
    ['en'] = {
        -- Messages
        ['timeouted'] = 'You need to wait at least 30 seconds before sending new message!',
        ['enter_message'] = 'Please enter message!',
        ['cant_answer'] = 'You are not able to answer this call!',
        ['no_call'] = 'This call does not exist!',
        ['arguments'] = 'Please enter all arguments needed!',
        ['not_able'] = 'You are not able to do that action!',
        ['no_permission'] = 'You are not able to use this command!',

        -- Dispatch call
        ['phone_call'] = 'Phone call',
        ['phone_call_code'] = 'XXX',

        -- Dispatch call answer message
        ['call'] = 'CALL',
        ['call_answered'] = 'Your call [%s] is being responded to!',
        ['call_answer'] = 'CALL [%s] ANSWER | %s',

        -- Chat suggestions
        ['send_call'] = 'Send call to specific faction.',
        ['answer_call'] = 'Answer to faction call.',

        -- Close/open
        ['open'] = 'Open dispatch',
        ['close_button'] = '[%s] Close dispatch',

        -- UI
        ['callsign'] = 'Callsign',
        ['name'] = 'Name',
        ['just_now'] = 'Just now',
        ['seconds_ago'] = 'seconds ago',
        ['about_minute'] = 'About minute ago';
        ['minutes'] = 'minutes ago',
        ['today'] = 'Today',
        ['yesterday'] = 'Yesterday',
        ['year'] = 'This year',

        ['log_text'] = 'ID: %s / TITLE: %s / JOBS: %s',
        ['log_author'] = 'ID: %s; IDENTIFIER: %s; NAME: %s'
    },

    ['et'] = {
        -- Messages
        ['timeouted'] = 'Peate ootama vähemalt 30 sekundit enne uue sõnumi saatmist!',
        ['enter_message'] = 'Palun sisestage sõnum!',
        ['cant_answer'] = 'Te ei saa sellele kõnele vastata!',
        ['no_call'] = 'See kõne ei eksisteeri!',
        ['arguments'] = 'Palun sisestage kõik vajalikud argumendid!',
        ['not_able'] = 'Te ei saa seda tegevust teha!',
        ['no_permission'] = 'Teil pole lubatud seda käsku kasutada!',

        -- Dispatch call
        ['phone_call'] = 'Telefonikõne',
        ['phone_call_code'] = 'KÕNE',

        -- Dispatch call answer message
        ['call'] = 'KUTSE',
        ['call_answered'] = 'Teie kutsele [%s] vastatakse!',
        ['call_answer'] = 'KUTSE [%s] VASTUS | %s',

        -- Chat suggestions
        ['send_call'] = 'Saada kutse fraktsioonile.',
        ['answer_call'] = 'Vasta fraktsiooni kutsele.',

        -- Close/open
        ['open'] = 'Ava dispatch',
        ['close_button'] = '[%s] Sulge dispatch',

        -- UI
        ['callsign'] = 'Kutsung',
        ['name'] = 'Nimi',
        ['just_now'] = 'Just nüüd',
        ['seconds_ago'] = 'sekundit tagasi',
        ['about_minute'] = 'Umbes minut tagasi',
        ['minutes'] = 'minutit tagasi',
        ['today'] = 'Täna',
        ['yesterday'] = 'Eile',
        ['year'] = 'Sellel aastal',

        ['log_text'] = 'ID: %s / PEALKIRI: %s / FRAKTSIOONID: %s',
        ['log_author'] = 'ID: %s; PID: %s; NIMI: %s'
    }
}

function isFactionAllowed(faction)
    return cfg.allowedFactions[faction]
end

function isRadioFaction(faction)
    if cfg.allowedFactions[faction] then
        return cfg.allowedFactions[faction].radio
    end

    return false
end

function debug_print(msg)
    if cfg.debug then
        print(msg)
    end
end
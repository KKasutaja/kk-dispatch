local framework = 'NONE'

CreateThread(function()
    if GetResourceState('es_extended') == 'starting' or GetResourceState('es_extended') == 'started' then
        framework = 'ESX'
    end

    if GetResourceState('qb-core') == 'starting' or GetResourceState('qb-core') == 'started' then
        framework = 'QBCORE'
    end
    
    if GetResourceState('kk-core') == 'starting' or GetResourceState('kk-core') == 'started' then
        framework = 'KKF'
    end
end)

CreateThread(function()
    if framework == 'ESX' then
        ESX = exports['es_extended']:getSharedObject()

        function loadPlayerData()
            return ESX.GetPlayerData()
        end

        function isPlayerDuty()
            return true
        end

        RegisterNetEvent('esx:playerLoaded', function(playerData)
            TriggerEvent('kk-dispatch:client:jobUpdate', playerData.job)
        end)

        RegisterNetEvent('esx:setJob', function(job)
            TriggerEvent('kk-dispatch:client:jobUpdate', job)
        end)

        function showInteraction(msg)
            ESX.TextUI(msg, 'info')
        end
    
        function hideInteraction()
            ESX.HideUI()
        end

        function openHandler() -- your own functions to allow/disallow opening
            return loadPlayerData().IsPlayerLoaded
        end
    elseif framework == 'QBCORE' then
        QBCore = exports['qb-core']:GetCoreObject()

        function loadPlayerData()
            return QBCore.Functions.GetPlayerData()
        end

        function isPlayerDuty()
            local Player = loadPlayerData()

            return Player.job.onduty
        end

        RegisterNetEvent('QBCore:Client:OnPlayerLoaded', function()
            Wait(1000)
            TriggerEvent('kk-dispatch:client:jobUpdate', loadPlayerData().job)
        end)

        RegisterNetEvent('QBCore:Player:SetPlayerData', function(playerData)
            TriggerEvent('kk-dispatch:client:jobUpdate', playerData.job)
        end)    

        RegisterNetEvent('QBCore:Server:OnJobUpdate', function(job)
            TriggerEvent('kk-dispatch:client:jobUpdate', job)
        end)

        function showInteraction(msg)
            exports['qb-core']:DrawText(msg, 'left')
        end
    
        function hideInteraction()
            exports['qb-core']:HideText()
        end

        function openHandler() -- your own functions to allow/disallow opening
            TriggerEvent('kk-dispatch:client:jobUpdate', loadPlayerData().job)

            return LocalPlayer.state.isLoggedIn
        end
    elseif framework == 'KKF' then
        KKF = exports['kk-core']:getSharedObject()

        local onDuty = false

        function loadPlayerData()
            return KKF.GetPlayerData()
        end

        function isPlayerDuty()
            return onDuty
        end

        RegisterNetEvent('KKF.Player.Loaded', function(playerData)
            TriggerEvent('kk-dispatch:client:jobUpdate', playerData.job)
            onDuty = playerData.job.onDuty
        end)

        RegisterNetEvent('KKF.Player.JobUpdate', function(job)
            TriggerEvent('kk-dispatch:client:jobUpdate', job)
            onDuty = job.onDuty
        end)

        RegisterNetEvent('KKF.Player.DutyChange', function(value) 
            onDuty = value
        end)

        function showInteraction(msg)
            KKF.ShowInteraction(msg, 'info')
        end
    
        function hideInteraction()
            KKF.HideInteraction()
        end

        function openHandler() -- your own functions to allow/disallow opening
            return LocalPlayer.state.isLoggedIn
        end
    end
end)
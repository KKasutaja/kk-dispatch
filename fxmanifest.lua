fx_version 'cerulean'
game 'gta5'

author 'KKasutaja'
description 'KK Dispatch'
version '1.0.0'

dependency 'ox_lib'

ui_page 'web/index.html'

shared_scripts {
    '@ox_lib/init.lua',
    'config.lua'
}

client_scripts {
    'client/framework.lua',
    'client/encrypted.lua'
}

server_scripts {
    'server/framework.lua',
    'server/encrypted.lua'
}

escrow_ignore {
    'config.lua',
    'client/framework.lua',
    'server/framework.lua'
}

files {
    'web/**'
}

lua54 'yes'
dependency '/assetpacks'